# markdown 语法
> 兼容HTML

[参考地址](http://www.markdown.cn)

#### 1. 标题
```log
# 这是 H1
## 这是 H2
###### 这是 H6
```
#### 2. 区块引用
```log
> This is a blockquote with two paragraphs. Lorem ipsum dolor sit amet,
> consectetuer adipiscing elit. Aliquam hendrerit mi posuere lectus.
> Vestibulum enim wisi, viverra nec, fringilla in, laoreet vitae, risus.
> 
> Donec sit amet nisl. Aliquam semper ipsum sit amet velit. Suspendisse
> id sem consectetuer libero luctus adipiscing.

```
#### 3. 列表
> 1. 无序列表使用星号、加号或是减号作为列表标记
>       *   Red
>       *   Red
>       *   Red
>
>2. 有序列表则使用数字接着一个英文句点
>       1. red
>       2. red
>       3. red
>
#### 4. 代码区块
```log
只要简单地缩进 4 个空格或是 1 个制表符就可以
```
    var s =1
#### 5. 代码区块指定语法
```css
{
    background-color: red;
}
```
```javascript
function plus(a,b) {
  return a+ b;
}
```
#### 6. 链接和图片
```log
链接：         []()
图片：         ![]()
自动连接：     <>
```
[参考地址](http://www.markdown.cn)

![alt](image/love.jpg '')

<http://example.com/>
#### 7. 代码
```log
1. ``
2. <code></code>
```
Use the `printf()` function.

<code>printf()</code>

#### 8. 字体（只对英文有效bug）
```log
**double asterisks**
*这是倾斜的文字*
_这是倾斜的文字_
***这是斜体加粗的文字***
~~这是加删除线的文字~~
```
**asterisks**<br>
*asterisks*<br>
***asterisks***
#### 9. 表格
```log

```

#### 9. 分隔线
```log
用三个以上的星号、减号、底线来建立一个分隔线，行内不能有其他东西。你也可以在星号或是减号中间插入空格
```
* * *
***
- - -
---

    

