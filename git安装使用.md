# git 安装使用

#### 1. 下载 安装
+ [下载地址 https://www.git-scm.com/download/](https://www.git-scm.com/download/)<br>
+ 按照步骤安装![图片](image/git-1.jpg)
#### 2. 右键菜单和git版本
``桌面右击菜单``<br>
``GIT GUI Here``<br>
```GIT Bash Here (选我)```
```log
输入：git --version
结果：git version 2.19.1.windows.1
```
#### 3. 配置 git global 设置
```log
git config --global user.name "yourname"   
git config --global user.email xxxxx@qq.com
```
#### 4. git 相关命令
```log
查看帮助文档
    git --help
clone线上项目
    git clone https://gitee.com/pseason/programming_software_install.git
查看提交历史
    git log
添加文件到版本库
    git add -A
查看变更情况
    git status
提交到本地仓库
    git commit -m ‘提交信息’
更新仓库
    git pull
提交到版本库
    git push
等等...
```
#### 5. .gitignore 文件
```log
.gitignore 过滤文件不让文件添加到版本库（针对编辑器生成文件、打包文件、依赖文件）
eg：
    .idea           过滤.idea整个文件夹
    node_modules    过滤node_modules整个文件夹
    *.iml           过滤所有以iml结尾的文件
```
+ 结束